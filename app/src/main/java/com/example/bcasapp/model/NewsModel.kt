package com.example.bcasapp.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize //di anotasikan untuk bawa data ke activity lainnya
data class NewsModel(
    val image: Int?,
    val title: String,
    val subTitle: String?,
    val category: String
) : Parcelable
