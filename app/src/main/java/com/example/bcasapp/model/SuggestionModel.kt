package com.example.bcasapp.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
class SuggestionModel(
    val images: Int?,
    val titles: String?,
    val subTitles: String?
): Parcelable