package com.example.bcasapp.model


data class CategoryModel (
    val id: Int?,
    val title: String?,
    val isSelected: Boolean?
)