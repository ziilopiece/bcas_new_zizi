package com.example.bcasapp.sharedPref

class Companion {

    companion object {
        val KEY_IS_LOGIN = "isLogin"
        val KEY_NAME = "name"
        val KEY_PASSWORD = "password"
        val KEY_EMAIL = "email"
        val KEY_NUMBER = "number"
    }

}