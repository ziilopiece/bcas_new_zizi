package com.example.bcasapp.detail

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.bcasapp.databinding.ActivityDetailNewsBinding
import com.example.bcasapp.model.NewsModel

class DetailNewsActivity : AppCompatActivity() {
    private lateinit var binding:ActivityDetailNewsBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailNewsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setDataToViewDetail()
    }

    private fun setDataToViewDetail() {
        val data = intent.getParcelableExtra<NewsModel>(DATA_NEWS)
        binding.tvDetailTitleNews.text = data?.title
        binding.tvDetailSubtitleNews.text = data?.subTitle
        binding.ivDetailNews.setImageResource(data?.image ?: 0)

        binding.back.setOnClickListener {
            onBackPressed()
        }
    }

    //companion object untuk mengambil variable dari class tersebut
    companion object {
        private const val DATA_NEWS = "datanews"

        //scooping function
        fun navigateToActivityDetail(activity: Activity, dataNews: NewsModel) {
            val intent = Intent(activity, DetailNewsActivity::class.java)
            intent.putExtra(DATA_NEWS, dataNews)
            activity.startActivity(intent)
        }

    }

}