package com.example.bcasapp.detail

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.bcasapp.databinding.ActivityDetailSuggestionBinding
import com.example.bcasapp.model.SuggestionModel

class DetailSuggestionActivity : AppCompatActivity() {
    private lateinit var binding: ActivityDetailSuggestionBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityDetailSuggestionBinding.inflate(layoutInflater)
        setContentView(binding.root)

        setDataToViewDetailSuggestion()
    }

    private fun setDataToViewDetailSuggestion() {
        val data = intent.getParcelableExtra<SuggestionModel>(DATA_SUGGESTION)
        binding.tvDetailTitleSuggestion.text = data?.titles
        binding.tvDetailSubtitleSuggestion.text = data?.subTitles
        binding.ivDetailSuggestion.setImageResource(data?.images ?: 0)

        binding.back.setOnClickListener {
            onBackPressed()
        }
    }

    //companion object untuk mengambil variable dari class tersebut
    companion object {
        private const val DATA_SUGGESTION = "datasuggestion"

        //scooping function
        fun navigateToActivityDetailSuggestion(activity: Activity, dataSuggestion: SuggestionModel) {
            val intent = Intent(activity, DetailSuggestionActivity::class.java)
            intent.putExtra(DATA_SUGGESTION, dataSuggestion)
            activity.startActivity(intent)
        }

    }
}