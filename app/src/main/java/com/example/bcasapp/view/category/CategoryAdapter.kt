package com.example.bcasapp.view.category

import android.content.res.ColorStateList
import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.example.bcasapp.R
import com.example.bcasapp.databinding.ItemCategoryBinding
import com.example.bcasapp.model.CategoryModel

class CategoryAdapter() : RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder> () {
    private var dataCategory: MutableList<CategoryModel> = mutableListOf()
    private var onClickList: (CategoryModel) -> Unit = {}

    fun setData(newData: MutableList<CategoryModel>) {
        dataCategory = newData
        notifyDataSetChanged()
    }

    fun addOnClickCategoryItem(clickCategory: (CategoryModel) -> Unit){
        onClickList = clickCategory
    }

    inner class CategoryViewHolder(
        val binding: ItemCategoryBinding
        ): RecyclerView.ViewHolder(binding.root){
            fun bindingData(data: CategoryModel) {
                binding.tvCategory.text = data.title
                binding.constraintCategory.setOnClickListener {
                    onClickList(data)
                }

                val (selectedBackgroundRes, selectedColor) = if (data.isSelected ?: false){
                    Pair(R.drawable.background_otline_black, Color.GREEN)
                }else{
                    Pair(R.drawable.background_rounded_selected, Color.GRAY)
                }

                val selectedBackground = ContextCompat.getDrawable(binding.root.context, selectedBackgroundRes)

                binding.constraintCategory.background = selectedBackground
                binding.tvCategory.setTextColor(ColorStateList.valueOf(selectedColor))

            }
        }


    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CategoryViewHolder = CategoryViewHolder (
        ItemCategoryBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
    )

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        holder.bindingData(dataCategory[position])

    }

    override fun getItemCount(): Int {
        return dataCategory.size
    }
}