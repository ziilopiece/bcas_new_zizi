package com.example.bcasapp.view.register

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.bcasapp.view.home.HomeActivity
import com.example.bcasapp.databinding.ActivityRegisterBinding
import com.example.bcasapp.sharedPref.Companion.Companion.KEY_EMAIL
import com.example.bcasapp.sharedPref.Companion.Companion.KEY_NAME
import com.example.bcasapp.sharedPref.Companion.Companion.KEY_PASSWORD
import com.example.bcasapp.sharedPref.ShPreferences
import com.example.bcasapp.view.login.LoginActivity
import com.example.bcasapp.view.profile.ProfileActivity

class RegisterActivity : AppCompatActivity() {
    private lateinit var binding: ActivityRegisterBinding
    lateinit var sharedPref: ShPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRegisterBinding.inflate(layoutInflater)
        setContentView(binding.root)
        sharedPref = ShPreferences(this)
        binding.btnRegister.setOnClickListener {
            val inputFullName = binding.etFullname.text.toString()
            val inputEmail = binding.etInputEmail.text.toString()
            val inputPassword = binding.etInputPassword.text.toString()

            if (inputFullName.isNullOrEmpty() || inputEmail.isNullOrEmpty() || inputPassword.isNullOrEmpty()) {
                Toast.makeText(applicationContext, "Data tidak boleh kosong", Toast.LENGTH_LONG)
                    .show()
            } else {

                val intent = Intent(this, LoginActivity::class.java)

                sharedPref.put(KEY_NAME, inputFullName)
                sharedPref.put(KEY_EMAIL, inputEmail)
                sharedPref.put(KEY_PASSWORD, inputPassword)

                Toast.makeText(applicationContext, "Registrasi Berhasil", Toast.LENGTH_LONG).show()
                startActivity(intent)
            }
        }

        binding.back.setOnClickListener {
            onBackPressed()
        }
    }
}