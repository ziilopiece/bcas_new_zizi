package com.example.bcasapp.view.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.bcasapp.databinding.ItemNewsBinding
import com.example.bcasapp.databinding.ItemSuggestionBinding
import com.example.bcasapp.model.NewsModel
import com.example.bcasapp.model.SuggestionModel

class HomeAdapter : RecyclerView.Adapter<HomeAdapter.HomeViewHolder>() {
    private var dataNews: MutableList<NewsModel> = mutableListOf()
    private var onClickNews: (NewsModel) -> Unit = {}

    fun addOnClickNews(clickNews: (NewsModel) -> Unit) {
        onClickNews = clickNews

    }


    fun addDataNews(newData: MutableList<NewsModel>) {
        dataNews.addAll(newData)
        notifyDataSetChanged()
    }

    fun setData(newData: MutableList<NewsModel>) {
        dataNews = newData
        notifyDataSetChanged()
    }


    inner class HomeViewHolder(val binding: ItemNewsBinding) : RecyclerView.ViewHolder(
        binding.root
    ) {
        fun bindView(data: NewsModel, onClickNews: (NewsModel) -> Unit) {
            binding.tvTitle.text = data.title
            binding.tvDescNews.text = data.subTitle
            binding.ivItem.setImageResource(data.image ?: 0)

            binding.llNews.setOnClickListener {
                onClickNews(data)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeAdapter.HomeViewHolder =
        HomeViewHolder(
            ItemNewsBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )

    override fun onBindViewHolder(holder: HomeAdapter.HomeViewHolder, position: Int) {
        holder.bindView(dataNews[position], onClickNews)
    }

    override fun getItemCount(): Int = dataNews.size
}