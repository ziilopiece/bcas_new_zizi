package com.example.bcasapp.view.login

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.bcasapp.view.home.HomeActivity
import com.example.bcasapp.view.register.RegisterActivity
import com.example.bcasapp.databinding.ActivityLoginBinding
import com.example.bcasapp.sharedPref.Companion.Companion.KEY_EMAIL
import com.example.bcasapp.sharedPref.Companion.Companion.KEY_NAME
import com.example.bcasapp.sharedPref.Companion.Companion.KEY_PASSWORD
import com.example.bcasapp.sharedPref.ShPreferences
import com.example.bcasapp.view.profile.ProfileActivity
import java.nio.channels.InterruptedByTimeoutException

class LoginActivity : AppCompatActivity() {
    private lateinit var binding: ActivityLoginBinding
    lateinit var sharedPref: ShPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)
        sharedPref = ShPreferences(this)

        val name = sharedPref.getString(KEY_NAME)
        val password = sharedPref.getString(KEY_PASSWORD)
        val email = sharedPref.getString(KEY_EMAIL)

        binding.tvRegister.setOnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
        }


        binding.btnLogin.setOnClickListener {
//            val email = "zizi@gmail.com"
            val inputUsername = binding.etUsername.text.toString()
            val inputPassword = binding.etPassword.text.toString()
            if (inputUsername == name && inputPassword == password){
                val intent = Intent(this, ProfileActivity::class.java)
                intent.putExtra(KEY_NAME, name)
                intent.putExtra(KEY_PASSWORD, password)
                intent.putExtra(KEY_EMAIL, email)
                startActivity(intent)
            }else{
                Toast.makeText(applicationContext, "Data yang anda masukkan salah", Toast.LENGTH_LONG).show()
            }
        }


    }

//    private fun navigateScreenWithInput(screen: Class<*>, input: String) {
//        val intent = Intent(applicationContext, screen)
//        intent.putExtra(KEY_NAME, input)
//        intent.putExtra(KEY_PASSWORD, input)
//        startActivity(intent)
//    }


}