package com.example.bcasapp.view.home

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import com.example.bcasapp.R
import com.example.bcasapp.databinding.ActivityHomeBinding
import com.example.bcasapp.detail.DetailNewsActivity
import com.example.bcasapp.detail.DetailSuggestionActivity
import com.example.bcasapp.model.CategoryModel
import com.example.bcasapp.model.NewsModel
import com.example.bcasapp.model.SuggestionModel
import com.example.bcasapp.view.category.CategoryAdapter
import com.example.bcasapp.view.suggestion.SuggestionAdapter

class HomeActivity : AppCompatActivity() {
    private var binding: ActivityHomeBinding? = null
    private val mainAdapter = HomeAdapter()
    private val categoryAdapter = CategoryAdapter()
    private val suggestionAdapter = SuggestionAdapter()



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding?.root)

        mainAdapter.setData(populateData())
        categoryAdapter.setData(populateData2())


        binding?.rvNewsVertical?.adapter = mainAdapter
        binding?.etSearch?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun afterTextChanged(p0: Editable?) {
                val dataN = populateData()
                val filter = dataN.filter { dataNews ->
                    dataNews.title.contains(p0.toString(), ignoreCase = true)
                            || dataNews.category.contains(p0.toString(), ignoreCase = true)
                }
                mainAdapter.setData(filter.toMutableList())

                val editTextValue = binding?.etSearch?.text
                if (editTextValue?.isEmpty() == true) {
                    binding?.ivClose?.visibility = View.GONE
                    binding?.rvListCategoryHorizontal?.visibility = View.VISIBLE
                } else {
                    binding?.ivClose?.visibility = View.VISIBLE
                    binding?.rvListSuggestionHorizontal?.visibility = View.INVISIBLE
                }
            }
        })

        binding?.rvListCategoryHorizontal?.adapter = categoryAdapter
        categoryAdapter.addOnClickCategoryItem { categoryModel ->
            val categoryData = populateData2()
            val category = categoryData.map {
                val categoryId = categoryModel.id
                val isSelected = it.id == categoryId
                it.copy(isSelected = isSelected)
            }
            categoryAdapter.setData(category.toMutableList())

            val data = populateData()
            val filterData = data.filter { dataNews ->
                dataNews.category == categoryModel.title
            }
            mainAdapter.setData(filterData.toMutableList())
        }

        binding?.ivClose?.setOnClickListener {
            binding?.etSearch?.setText("")
        }

        binding?.rvListSuggestionHorizontal?.adapter = suggestionAdapter
        val data2 = suggestion()
        suggestionAdapter.addDataSuggestion(data2)

        binding?.rvNewsVertical?.adapter = mainAdapter
        val data = populateData()
        mainAdapter.addDataNews(data)

        mainAdapter.addOnClickNews { dataNews ->
            DetailNewsActivity.navigateToActivityDetail(this, dataNews)
        }

        suggestionAdapter.addOnClickSuggestion { dataSuggestion ->
            DetailSuggestionActivity.navigateToActivityDetailSuggestion(this, dataSuggestion)
        }

        binding?.back?.setOnClickListener {
            onBackPressed()
        }

    }


    private fun populateData2(): MutableList<CategoryModel> {
        val listdata = mutableListOf(
            CategoryModel(
                id = 1,
                title = "Fashion",
                isSelected = false
            ),
            CategoryModel(
                id = 2,
                title = "Movies",
                isSelected = false
            ),
            CategoryModel(
                id = 3,
                title = "Healthy",
                isSelected = false
            ),
            CategoryModel(
                id = 4,
                title = "Sports",
                isSelected = false
            ),
            CategoryModel(
                id = 5,
                title = "Culinery",
                isSelected = false
            ),
            CategoryModel(
                id = 6,
                title = "Electronics",
                isSelected = false
            ),

            )
        return listdata
    }

    private fun populateData(): MutableList<NewsModel> {
        val listData = mutableListOf(
            NewsModel(
                image = R.drawable.larva,
                title = "Semeru Mengeluarkan Larva Malam ini",
                subTitle = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                category = "Electronics"
            ),
            NewsModel(
                image = R.drawable.flood,
                title = "Banjir di Jakarta",
                subTitle = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                category = "Culinery"
            ),
            NewsModel(
                image = R.drawable.tsunami,
                title = "Gelombang Tsunami Menghantam Pulau Kecil",
                subTitle = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                category = "Sports"
            ),
            NewsModel(
                image = R.drawable.tenggelam,
                title = "Seoarang Anak Ditemukan Tenggelam di Sungai",
                subTitle = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                category = "Healthy"
            ),
            NewsModel(
                image = R.drawable.eathquake,
                title = "Gempa Kembali Mengguncang Jakarta",
                subTitle = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                category = "Electronics"
            ),

            NewsModel(
                image = R.drawable.cat,
                title = "Pengadilan Agama Memutuskan Sambo di Hukum Mati",
                subTitle = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                category = "Electronics"
            ),

            NewsModel(
                image = R.drawable.flood,
                title = "Banjir di Solo",
                subTitle = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
                category = "Electronics"
            ),

            )
        return listData
    }

    private fun suggestion(): MutableList<SuggestionModel> {
        val listData = mutableListOf(
            SuggestionModel(
                images = R.drawable.larva,
                titles = "Bla bla bla bla",
                subTitles = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
            ),
            SuggestionModel(
                images = R.drawable.larva,
                titles = "Banjir Sedalam 5 Meter di Kampung C",
                subTitles = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
            ),
            SuggestionModel(
                images = R.drawable.larva,
                titles = "Banjir Sedalam 5 Meter di Kampung C",
                subTitles = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
            ),
            SuggestionModel(
                images = R.drawable.larva,
                titles = "Gempa Mengguncang Kawasan Lombok",
                subTitles = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
            ),
            SuggestionModel(
                images = R.drawable.larva,
                titles = "Rumah Berterbangan Karena Tornado",
                subTitles = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum."
            )
        )
        return listData
    }

    override fun onDestroy() {
        super.onDestroy()
        binding = null
    }

    private fun intentTo(clazz: Class<*>) {
        val intent = Intent(this, clazz)
        startActivity(intent)
    }

}