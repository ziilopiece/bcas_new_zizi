package com.example.bcasapp.view.suggestion

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.bcasapp.databinding.ItemSuggestionBinding
import com.example.bcasapp.model.NewsModel
import com.example.bcasapp.model.SuggestionModel

class SuggestionAdapter: RecyclerView.Adapter<SuggestionAdapter.SuggestionViewHolder>() {
    private var dataSuggestion: MutableList<SuggestionModel> = mutableListOf()

    private var onClickSuggestion: (SuggestionModel) -> Unit = {}

    fun addOnClickSuggestion(clickSuggestion: (SuggestionModel) -> Unit){
        onClickSuggestion = clickSuggestion

    }

    fun addDataSuggestion(newData: MutableList<SuggestionModel>) {
        dataSuggestion.addAll(newData)
        notifyDataSetChanged()
    }
    inner class SuggestionViewHolder(val binding: ItemSuggestionBinding) : RecyclerView.ViewHolder(
        binding.root
    ) {
        fun bindData(data: SuggestionModel, onClickSuggestion: (SuggestionModel) -> Unit) {
            binding.tvTitleSuggestion.text = data.titles
            binding.tvDescSuggestion.text = data.subTitles
            binding.ivSuggestion.setImageResource(data.images ?: 0)

            binding.llSuggestion.setOnClickListener {
                onClickSuggestion(data)
            }
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SuggestionViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = ItemSuggestionBinding.inflate(layoutInflater, parent, false)
        return SuggestionViewHolder(binding)
    }

    override fun getItemCount(): Int = dataSuggestion.size

    override fun onBindViewHolder(holder: SuggestionViewHolder, position: Int) {
        holder.bindData(dataSuggestion[position], onClickSuggestion)
    }
}