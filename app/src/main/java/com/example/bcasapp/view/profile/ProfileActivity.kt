package com.example.bcasapp.view.profile

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.bcasapp.databinding.ActivityProfileBinding
import com.example.bcasapp.sharedPref.Companion.Companion.KEY_EMAIL
import com.example.bcasapp.sharedPref.Companion.Companion.KEY_NAME
import com.example.bcasapp.sharedPref.Companion.Companion.KEY_PASSWORD
import com.example.bcasapp.sharedPref.ShPreferences
import com.example.bcasapp.view.home.HomeActivity
import com.example.bcasapp.view.login.LoginActivity

class ProfileActivity : AppCompatActivity() {
    private lateinit var binding: ActivityProfileBinding
    lateinit var sharedPref: ShPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)
        sharedPref = ShPreferences(this)

        val name = sharedPref.getString(KEY_NAME)
        val password = sharedPref.getString(KEY_PASSWORD)
        val email = sharedPref.getString(KEY_EMAIL)

        binding.tvName.text = name
        binding.tvPassword.text = password
        binding.tvEmail.text = email

        binding.btnLogout.setOnClickListener {
            val intent = Intent(applicationContext, LoginActivity::class.java)
            Toast.makeText(applicationContext, "Berhasil Logout", Toast.LENGTH_LONG).show()
            startActivity(intent)
            finish()
        }

        binding.ivHomeMenu.setOnClickListener {
            val intent = Intent(this, HomeActivity::class.java)
            startActivity(intent)

        }


    }
}

